<?php

/**
 * Class PerchFieldType_Calendar
 *
 * A fieldtype for Perch allowing editors to select dates 
 * using a Flatpickr.js calendar and output them in templates
 * in different ways. Supports single, multiple and range dates.
 * 
 * Examples:
 *
 * Single date default: 
 * <perch:content id="date" type="calendar" label="Select a date" />
 *
 * Single with time and default altformat (show the user a readable date):
 * <perch:content id="dateTime" type="calendar" label="Select date and time" altformat time  />
 *
 * Multiple dates with time and custom altformat. Output as html list: 
 * <perch:content id="dateMultiple" type="calendar" label="Select dates and time" time altformat="j M H:i" output="list"/>
 *
 * Date range: Output formatted dates for start and end separately:
 * <perch:content id="dateRange" type="calendar" label="Select date range" mode="range" output="start" format="D d F y" />
 * <perch:content id="dateRange" type="calendar" label="Select date range" mode="range" output="end" format="D d F y" />
 *
 * @author  Tim Kinali <tim@tamburlane.se>
 * @version 2.0.0
 *
 * @credits 
 * Forked from https://gist.github.com/byron-roots/7ec3f25657fc6777cdcf76667e401dfd
 * author Byron Fitzgerald <byron@rootstudio.co.uk>
 * This version adds more configuration options, output options, 
 */
 
 
class PerchFieldType_calendar extends PerchAPI_FieldType {

		public function add_page_resources() {		
			$Perch = Perch::fetch();
			$Perch->add_javascript(PERCH_LOGINPATH . '/addons/fieldtypes/calendar/calendar-fieldtype.js');
			$Perch->add_css('https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css');
		}


		public function render_inputs($details = []) {
		/**
			* Render the input in the admin interface
			*
			* @param array $details
			*
			* @return string
			*/
				// INPUT FIELD
				$input_attr = 'data-input';
				$class = 'text input-simple ';
				
				// Size
				if ($this->Tag->size) {
						$class .= " {$this->Tag->size}";
				} else {
						$class .= ' m';
				}
				
				// Placeholder
				if ($this->Tag->placeholder) {
						$input_attr .= " placeholder='{$this->Tag->placeholder}'";
				} else {
					$input_attr .= " placeholder='Please choose …'";
				}

				// FLATPICKR WRAPPER
				
				$attr = 'data-time_24hr=true data-wrap=true';

				// Mode
				if ($this->Tag->mode && $this->Tag->mode == 'range') {
					$attr .= ' data-range';	
				}
				elseif ($this->Tag->mode && $this->Tag->mode == 'multiple') {
					$attr .= ' data-multiple';
				}
				// Dateformat
				if ($this->Tag->dateformat) {
					$attr .= " data-date-format='{$this->Tag->dateformat}'";
				}	else {
					$attr .= ' data-date-format=';
					$attr .= $this->Tag->time ? "'Y-m-d H:i:S'" : "'Y-m-d'";
				}				
				// Altformat
				// If altformat=true, set a opinionated default altformat.
				if (PerchUtil::bool_val($this->Tag->altformat)) {
						$attr .= " data-alt-input=true data-alt-format=";
						$attr .= $this->Tag->time ? "'l j F Y (H:i)'" : "'l j F Y'";
				}
				// Custom altformat value set, use that instead
				elseif ($this->Tag->altformat) {
						$attr .= " data-alt-input=true data-alt-format='{$this->Tag->altformat}'";
				}	
				// Time
				if ($this->Tag->time) {
						$attr .= ' data-enable-time=true';
				}
				else {
					$attr .= ' data-enable-time=false';
				}
				
				// Create input
				$data  = isset($details[$this->Tag->input_id()]) ? $details[$this->Tag->input_id()] : array();
				$value = $this->Form->get($data, '_default', $this->Tag->default(), $this->Tag->post_prefix());				
				$input =	 $this->Form->text($this->Tag->input_id(), $value, $class, false, 'text', $input_attr);
				
				$icon = PerchUI::icon('core/cancel', 14, 'Clear');
				$clear = "<a style='cursor: pointer; color:#ddd; padding:4px; width:24px; height:24px; display:inline-block' title='clear' data-clear>{$icon}</a>";
				
				$output  = "<div class='flatpickr js-datepicker' {$attr}> {$input} {$clear} </div>";
				
				
				return $output;
				
		}


		public function get_raw($post = false, $Item = false) {
		/**
		* Get raw values to store in database
		*
		*/
				$store = [];
				
				$id = $this->Tag->id();
				
				if ($post === false) {
					$post = PerchRequest::post();
				}
				
				if (isset($post[$id])) {
					$this->raw_item = $post[$id];
					$dates = 	$this->raw_item;
				}
					
				if ($dates) {		
						
						$store['_default'] = $dates;
						
						// To do: Now using Flatpickr default. Allow custom by data-conjuntion attr.
						$separator = ($this->Tag->mode() == 'range') ? 'to' : ',';
						
						// Split up string of multiple dates to array, trim each element
						$date_parts = array_map('trim', explode($separator, $dates));
						
						//Needed for mode multiple
						sort($date_parts); 
						
						$count = PerchUtil::count($date_parts);						
						$start = $date_parts[0];
						$end = end($date_parts);
						
						// For single and multiple
						$store['duration'] = $count;
						
						switch($this->Tag->mode()) {
									
							case 'range':
								$store['mode'] 		= $this->Tag->mode; 			
								$store['start'] 		= $start;
								$store['end'] 			= $end;
								
								$startDate = new DateTime($start);
								$endDate = new DateTime($end);
								// Counting full days regardless of time
								$startDate->setTime(0, 0, 0);
								$endDate->setTime(0, 0, 0);								
								$diff = $startDate->diff($endDate);
								// Account for starting day (+1)
								$store['duration'] = $diff->days + 1;
								break;
							
							case 'multiple':
								$store['_default'] = implode( ", ", $date_parts ); //the sorted array, to string
								$store['mode'] 		= $this->Tag->mode; 	
								$store['start'] 		= $start;
								$store['end'] 			= $end;
								
								// Store array for convenient use with perch_content_custom
								// Also used in get_processed() to output json in template (output="json")
								$store['dates'] 		= $date_parts; 
								
								$list = "<ul class=\"calendar-dates-multiple\">";
								foreach ($date_parts as $listItem) {
									$list .= '<li>' .  PerchUtil::html($listItem, true) . '</li>';
								}
								$list .= "</ul>";
								$store['list'] 		=	$list;
								break;
								
						}
				}
				PerchUtil::debug($store, 'notice');
				
				return $store;
		}
		

		public function get_search_text($raw = false) {
			 if ($raw===false) $raw = $this->get_raw();
					if (!PerchUtil::count($raw)) return false;
	
					if (isset($raw['_default'])) return $raw['_default'];
	
			return false;
		}

		public function render_admin_listing($raw = false) {
				return PerchUtil::html($this->get_search_text($raw));
		}


		public function get_index($raw = false) {

			/**
			* Get values for filtering index
			* @param  boolean $raw [description]
			* @return [type]       [description]
			*/	
			
				$id = $this->Tag->id();

				$out = [];
				
				if (is_array($raw)) {

						if (isset($raw['_default'])) {
								$out[] = ['key' => $id, 'value' => trim($raw['_default'])];
						} else {
								if (isset($raw['processed'])) {
										$out[] = ['key' => $id, 'value' => trim($raw['processed'])];
								}
						}

						foreach ($raw as $key => $val) {
								if (!is_array($val)) {
										$out[] = ['key' => $id . '_' . $key, 'value' => trim($val)];
								}
						}

				} else {
						$out[] = ['key' => $id, 'value' => trim($raw)];
				}

				return $out;
		}
		

		public function get_processed($raw = false) {

		/**
		* Get data to return to template
		*
		*/
			
				if ($raw === false) $raw = $this->get_raw();

				if (is_array($raw)) {
					
					$item = $raw;
					 
					if ($this->Tag->output() && $this->Tag->output()!='single') {
						switch($this->Tag->output()) {
							
							case 'json':
							   return isset($item['dates']) ? PerchUtil::json_safe_encode($item['dates'], false) : false;
							break;
								 
							case 'list':
								$this->processed_output_is_markup = true;
								return isset($item['list']) ? $item['list'] : false;
							break;

							default:
								// mode, start, end, duration, dates(array)
								if (isset($item[$this->Tag->output()])) {
									return $item[$this->Tag->output()];
								}
							break;							
						} 
					 }
					
					return $item['_default'];	
				
				}
				return $raw;	

		}

		public function get_content_summary($details = [], $Template) {
				$id = $this->Tag->id();

				if (!PerchUtil::count($details)) return '';

				if (array_key_exists($id, $details)) {
						$raw = $details[$id];
						$value = '';

						if (is_array($raw)) {
								if (isset($raw['_title'])) {
										$value = trim($raw['_title']);
								}else if (isset($raw['_default'])) {
										$value = trim($raw['_default']);
								}else if (isset($raw['processed'])) {
										$value = trim($raw['processed']);
								}
						} else {
								$value = $raw;
						}

						if ($value) {
								$value = PerchUtil::excerpt($value, 12);
						}

						return PerchUtil::html($value, true);
				}

				return '';
		}
				
	

}