# Calendar Field Type

 A fieldtype for Perch allowing editors to select dates using a Flatpickr.js calendar. Supports single, multiple and range dates. In templates you can output dates in a few different ways. 


## Install by download

- Download and unzip
- Create a new folder named `calendar` in `perch/addons/fieldtypes`
- Place the contents of the download in `perch/addons/fieldtypes/calendar`

### Install as submodule

You can install the fieldtype as a submodule in your Git project. Try this command or check the documentation if you're using a git GUI client.
    
    $ git submodule add https://bitbucket.org/tamburlane/perch-fieldtype-calendar.git <perch_path>/addons/fieldtypes/calendar 

## Requirements

- Perch or Perch Runway 3.0 or higher


## Some usage examples

Single date default: 
```html
 <perch:content id="date" type="calendar" label="Select a date" />
``` 

Single date with time and default altformat (show the user a readable date):
```html
<perch:content id="dateTime" type="calendar" label="Select date and time" altformat time  />
```

Multiple dates with time and custom altformat. Output as html list: 
```html
<perch:content id="dateMultiple" type="calendar" label="Select dates and time" mode="multiple" time altformat="j M H:i" output="list"/>
```

Date range: Output formatted dates for start and end separately:
```html
<perch:content id="dateRange" type="calendar" label="Select date range" mode="range" output="start" format="D d F y" />
<perch:content id="dateRange" type="calendar" label="Select date range" mode="range" output="end" format="D d F y" />
```


## Attributes

| Attribute   | Default                               | Values               | Description                                                                                                                                                                                                                                                       |
|-------------|---------------------------------------|----------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| mode        | Single date                           | `multiple`, `range`  | Allow selection of multiple dates with`multiple`, or a range of dates with `range`.                                                                                                                                                                               |
| time        | false                                 | boolean              | If set, enables time picker and sets `dateformat` and `altformat` defaults to include time.                                                                                                                                                                                   |
| dateformat  | 'Y-m-d' or 'Y-m-d H:i:S'              | Token string         | Formatting tokens that defines how the date is stored, displayed and output in templates.                                                                                                                                                                         |
| altformat   | 'l j F Y' or 'l j F Y (H:i)'          | True or token string | Like dateformat, but only for the calendar. Useful to show the editor a readable date, but return something different to the server as defined by `dateformat`. Set to `true` or without a value to activate the defaults, or provide a custom formatting string. |
| placeholder | 'Please select …'                     | String               | Placeholder text in the date input when no date is selected.                                                                                                                                                                                                      |
| output      | String of date(s)                     | See output options   | Used to output the dates in different ways.                                                                                                                                                                                                                       |


### Output attribute options

| Value / key     | Use with            | Description                                               |
|-----------------|---------------------|-----------------------------------------------------------|
| none / _default | all modes           | Outputs the date(s) as a string.                          |
| duration        | all modes           | The count of dates (number of days)                       |
| start           | `multiple`, `range` | The earliest date                                         |
| end             | `multiple`, `range` | The latest date                                           |
| mode            | `multiple`, `range` | The mode being used (to use with custom functions in PHP) |
| list            | `multiple`          | HTML list of dates                                        |
| json            | `multiple`          | Array of dates as json (to process with javascript)       |
| dates           | `multiple`          | PHP array of dates (to use with custom functions in PHP)  |


## Formatting dates 

You have three ways to control how dates are formatted. 

Use `altformat` when you want to change _only_ how they appear in the form the editor sees. 

On the other hand, if you set a custom `dateformat` it will change how dates are stored. That also changes the output in templates. The same formatting will be used in the edit form as well, unless you are combining it with the `altformat` attribute.

[Available formatting tokens](https://flatpickr.js.org/formatting/)


### In templates
The template output of a date can usally be formatted to your needs with [Perch `format` attribute](https://docs.grabaperch.com/templates/attributes/format/). Note that if you are using a custom `dateformat`the Perch format attribute might not work. For that reason it's best for most cases to stick with the default `dateformat`. 

